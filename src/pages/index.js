import React from 'react'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import ProTip from '../components/ProTip'
import Link from '../components/Link'
import SEO from '../components/SEO'

export default function App() {
  return (
    <div>
      <SEO title="Home" />
      <Container maxWidth="md">
        <Box my={4}>
          <Typography variant="h4" component="h1" gutterBottom>
            Gatsby v4-beta example
          </Typography>
          <Link to="/dashboard" color="secondary">
            Go to the about page
          </Link>
          <ProTip />
        </Box>
      </Container>
    </div>
  )
}

