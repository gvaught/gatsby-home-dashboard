import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ProTip from '../components/ProTip';
import Link from '../components/Link';

export default function App() {
  return (
    <Container maxWidth="md">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          404
        </Typography>
        <Link to="/" color="secondary">
          Go home...
        </Link>
        <ProTip />
      </Box>
    </Container>
  );
}

