/* eslint-disable no-script-url */

import React from 'react'
import Typography from '@material-ui/core/Typography'
import OpenWeatherAPI from './API/OpenWeatherAPI'

export default function Weather() {
  return (
    <>
      <Typography component="h2" variant="h6" color="primary" gutterBottom>Current Weather</Typography>
        <OpenWeatherAPI
        />
    </>
  )
}
