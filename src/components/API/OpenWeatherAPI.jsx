import React from 'react'

export default class OpenWeatherAPI extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
      i: 0
    };
  }

  componentDidMount() {
    fetch('https://api.openweathermap.org/data/2.5/forecast?id=4684888&units=imperial&appid=4d754de6f96d1dc3ef0fad7748dd7967')
      .then(response => response.json())
      .then(
        (response) => {
          this.setState({
            isLoaded: true,
            data: response.list
          })
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, data } = this.state
    if (error) {
      return (
        <div>Error: {error.message}</div>
      )
    } else if (!isLoaded) {
      return (
        <div>Loading...</div>
      )
    } else if (isLoaded) {
      return (
        <div>
          {data.map((data, i) => (
            <span key={i}>
              {data.main.temp} °F
            </span>
          ))}
        </div>
      )
    }
  }
}
