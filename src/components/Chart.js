import React from 'react'
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts'
import Typography from '@material-ui/core/Typography'
// import Title from './Title';

// Generate Sales Data
function createData(time, amount) {
  return { time, amount }
}

const Chart = ({ data }) => {
  data = [
    createData('00:00', 0),
    createData('03:00', 300),
    createData('06:00', 600),
    createData('09:00', 800),
    createData('12:00', 1500),
    createData('15:00', 2000),
    createData('18:00', 2400),
    createData('21:00', 2400),
    createData('24:00', undefined),
  ]
  return (
    <>
      <Typography component="h2" variant="h6" color="primary" gutterBottom>
        Chart
      </Typography>
      <ResponsiveContainer>
        <LineChart
          data={data}
          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis dataKey="time" />
          <YAxis>
            <Label angle={270} position="left" style={{ textAnchor: 'middle', color: '#E1E1E1' }}>
              Sales ($)
            </Label>
          </YAxis>
          <Line type="monotone" dataKey="amount" stroke="#c95c5c" dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </>
  )
}

export default Chart
