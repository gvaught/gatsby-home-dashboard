import { red } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#c95c5c',
    },
    secondary: {
      main: '#eccce0',
    },
    text: {
      main: '#E1E1E1',
    },
    error: {
      main: red.A400,
      // default: red,
    },
    background: {
      dark: '#121212',
      main: '#121212',
    },
  },
});

export default theme;

